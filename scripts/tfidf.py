from time import time
import json

from tests.algos import find_similar_issues_tfidf
from tests.algos import store_issues
from IGitt.GitHub.GitHub import GitHubRepository
from IGitt.GitHub import GitHubToken
from igitt_django.models import IGittIssue

THRESHOLD_TEST_SET = [0.8, 0.85, 0.9, 0.95]

def get_issue(issue_no, token):
    """
    Retrieves the issue from the database.
    """
    issue = IGittIssue.objects.get(number=issue_no).to_igitt_instance(token)
    return {
        'number': issue.number,
        'title': issue.title,
        'description': issue.description,
    }

def test_tfidf_for_threshold(issues, token, threshold):
    dupes = set()
    results = []
    print('*' * 80)
    with open('output-%g.json' % threshold, 'a') as file:
        start = time()
        similar_issues = find_similar_issues_tfidf(issues, threshold)
        end = time()
        print('Time taken: {}'.format(end-start))
        for issue_compared_against, issue_dict in similar_issues.items():
            dupes.add(issue_compared_against)
            for issue_in_comparison, similarity in issue_dict.items():
                dupes.add(issue_in_comparison)
                print('Issue compared against: %d' % issue_compared_against)
                print(f'Similarity: %f' % similarity)
                print(f'Issue in comparison: %d' % issue_in_comparison)
                comparison = {
                    'against': get_issue(issue_compared_against, token),
                    'compared': get_issue(issue_in_comparison, token),
                }
                results.append(comparison)
        print('Threshold considered: %f' % threshold)
        print('Size of dupes: %d' % len(dupes))
        print('dupes: %s' % dupes)
        print('Check output-%g.json for more details.' % threshold)
        json.dump(results, file, ensure_ascii=False, indent=2)


def run(*args):
    """
    The main test routine.
    """
    token = GitHubToken(args[0])
    repo = GitHubRepository(token, args[1])
    issues = store_issues(repo, token)
    for threshold in THRESHOLD_TEST_SET:
        test_tfidf_for_threshold(issues, token, threshold)
