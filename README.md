# Duplicate Issue Detection

This is an approach of solving the problem of duplicate issues in repositories.
This repository holds several approaches tried at solving this.

- It uses igitt-django to fetch and store the test data in a postgresql
database, named `issue_dupe`.

## Testing

1. Install requirements

```bash
$ pip3 install -r requirements.txt
```

2. Run django migrations

```bash
$ python3 manage.py migrate
```

3. Run test script for tfidf

```bash
$ python3 manage.py runscript tfidf --traceback --script-args <your access token> <repo name>
```

> Note: Please make sure you use this in a virtual environment.
> And don't forget to install **postgresql** first.

> The data set used at the point of time of creation of this repo is available
> [here](https://www.dropbox.com/s/gudvb1jehjv8y90/data-coala-coala.json?dl=0).

## Outcome

The results for the test set on GitHub's [coala/coala](https://github.com/coala/coala)
can be seen [here](https://hackmd.io/JwNgxgzArApgHDAtAFhAMzC5YBGi5xhIDsIADDjmMVWjUA==#).
