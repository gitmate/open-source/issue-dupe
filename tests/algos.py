"""
This module contains all the algorithms developed under finding duplicate or
similar issues.
"""

from collections import defaultdict
import json
import os

from IGitt.GitHub import get
from IGitt.GitHub.GitHubIssue import GitHubIssue
from IGitt.Interfaces import Token
from IGitt.Interfaces.Issue import Issue
from IGitt.Interfaces.Repository import Repository
from sklearn.feature_extraction.text import TfidfVectorizer
import spacy

from igitt_django.models import IGittIssue


def store_issues(repo: Repository, token: Token):
    """
    Helper function to fetch and store the issues from a Repository object.

    :return:
        A set of all the stored issues from the Repository object.
    """
    print('Storing issues...')
    filename = 'data-{repo}.json'.format(repo=repo.full_name.replace('/', '-'))
    if os.path.exists(filename) and os.path.isfile(filename):
        print('Fetching data from %s' % filename)
        with open(filename, 'r') as file:
            data = json.load(file)
    else:
        print('Fetching data into %s' % filename)
        data = get(token, repo._url + '/issues', {'state': 'all'})
        with open(filename, 'w') as file:
            json.dump(data, file, indent=2, ensure_ascii=False)

    issues_data = list(filter(lambda x: 'pull_request' not in x, data))
    issues = [
        GitHubIssue.from_data(issue, token, repo.full_name, issue['number'])
        for issue in issues_data
    ]

    for issue in issues:
        IGittIssue.from_igitt_instance(issue)

    return issues

def get_issue_summary(issue):
    """
    Helper function to retrieve issue summary.
    """
    title, body = issue.title, issue.description
    issue_text = (title + ' ' + body)
    return issue_text.replace('\r', ' ').replace('\n', ' ')


################### SPACY BASED #######################

def find_similar_issues_spacy(issues: [Issue], threshold: int = 0.8):
    nlp = spacy.load('en')
    results = defaultdict(dict)
    for issue in issues:
        issue_text = get_issue_summary(issue)
        for _issue in issues:
            if _issue.number == issue.number:
                continue
            _issue_text = get_issue_summary(_issue)
            similarity = nlp(issue_text).similarity(nlp(_issue_text))
            if similarity > threshold:
                results[issue.number][_issue.number] = similarity
    return results

################### TF-IDF BASED ######################

def tfidf_similarity(X, Y=None):
    if Y is None:
        Y = X.T
    return (X * Y).A


def find_similar_issues_tfidf(issues: [Issue], threshold: int = 0.85):
    issue_numbers = [issue.number for issue in issues]
    issue_summaries = [get_issue_summary(issue) for issue in issues]

    vector = TfidfVectorizer(min_df=0, ngram_range=(1, 5))
    tfidf_sparse_matrix = vector.fit_transform(issue_summaries)
    matrix = tfidf_similarity(tfidf_sparse_matrix)

    # Map issue_numbers with respective scores
    results = [zip(issue_numbers, scores) for scores in matrix]
    results = [dict(filter(lambda r: r[1] >= threshold, row))
               for row in results]
    results = dict(zip(issue_numbers, results))

    # Clean results
    for issue_number in issue_numbers:
        # Remove similarity with itself
        if issue_number in results and issue_number in results[issue_number]:
            del results[issue_number][issue_number]
        # Delete empty entries
        if not results[issue_number]:
            del results[issue_number]
    # And we're done!
    return results
